<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Nibbus.com - Tu próximo hogar</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>
    <body>

    <div class="main container-fluid" id="app">

        <div class="row">
            <div class="col-md-12">
                <header>
                <nav class="navbar navbar-expand-sm fixed-top m-0 p-0">
                    <div class="container pl-0 pr-0">
                        <button type="button" class="navbar-toggler float-right" data-toggle="collapse" data-target="#navbarcCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarcCollapse">
                            <ul class="navbar-nav mr-auto ml-4">
                                <li class="nav-item mr-4"><a href="/" class="nav-link"><i class="far fa-home fa-fw mr-1"></i>Inicio</a></li>
                                <li class="nav-item mr-4"><a href="#" class="nav-link"><i class="far fa-building fa-fw mr-1"></i>proyectos</a></li>
                                <li class="nav-item mr-4"><a href="#" class="nav-link"><i class="far fa-briefcase fa-fw mr-1"></i>anunciantes</a></li>
                                <li class="nav-item mr-4"><a href="#" class="nav-link"><i class="far fa-cloud fa-fw mr-1"></i>preguntas frecuentes</a></li>
                                <li class="nav-item mr-4"><a href="#" class="nav-link"><i class="far fa-at fa-fw mr-1"></i>newsletter</a></li>
                                <li class="nav-item mr-4"><a href="#" class="nav-link"><i class="far fa-envelope fa-fw mr-1"></i>contáctanos</a></li>
                            </ul>
                        </div>
                        <a href="http://google.com" class="btn btn-primary publica">publica tu aviso</a>
                        <ul class="navbar-nav float-right pr-4">
                            <li class="nav-item mr-2"><a href="#" class="nav-link m-0 px-0"><img src="{{ asset('/img/ico_fb.svg') }}" class="svg" alt="Facebook"></a></li>
                            <li class="nav-item mr-2"><a href="#" class="nav-link m-0 px-0"><img src="{{ asset('/img/ico_tw.svg') }}" class="svg" alt="Twitter"></a></li>
                            <li class="nav-item mr-2"><a href="#" class="nav-link m-0 px-0"><img src="{{ asset('/img/ico_li.svg') }}" class="svg" alt="LinkedIn"></a></li>
                            <li class="nav-item mr-2"><a href="#" class="nav-link m-0 px-0"><img src="{{ asset('/img/ico_yt.svg') }}" class="svg" alt="Youtube"></a></li>
                            <li class="nav-item mx-0"><a href="#" class="nav-link m-0 px-0"><img src="{{ asset('/img/ico_in.svg') }}" class="svg" alt="Instagram"></a></li>
                        </ul>
                    </div>
                </nav>
                </header>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 slide m-0 p-0">
                <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="dark-overlay"></div>
                            <img class="d-block w-100" src="{{ asset('/img/slide1.jpg') }}" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <div class="dark-overlay"></div>
                            <img class="d-block w-100" src="{{ asset('/img/slide1.jpg') }}" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <div class="dark-overlay"></div>
                            <img class="d-block w-100" src="{{ asset('/img/slide1.jpg') }}" alt="Third slide">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('/js/app.js') }}"></script>
    </body>
</html>
